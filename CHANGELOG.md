Entwicklung unseres Projektes

Bisher haben wir uns nur überlegt, wie wir vorgehen; welche Schritte wir durchlaufen und uns Gedanken über die Aufgaben Verteilung gemacht.

Final Code:
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
struct Matrix
{
	float array[3][3];
};
struct Matrix matrixAddierer(float matrix1[3][3], float matrix2[3][3])
{
	struct Matrix summe;
	//Rechnung
	int i, j;
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			summe.array[i][j] = matrix1[i][j] + matrix2[i][j];
		}
	}
	return summe;
}
int main()
{
	srand(time(NULL));
	struct Matrix summe;
	int i, j, x, y;
	float anzahl;
	float matrix1[3][3];
	float matrix2[3][3];
	FILE *f;
	f = fopen("matrizen.txt", "r");
	fscanf(f, "%f", &anzahl);
	printf("Anzahl der Matrizen: %0.f\n", anzahl);
	printf("\n");
	for (i = 0; i < 3; i++)
	{
		fscanf(f, "%f %f %f", &matrix1[i][0], &matrix1[i][1], &matrix1[i][2]);
	}
	for (j = 1; j < anzahl; j++)
	{
		for (i = 0; i < 3; i++)
		{
			fscanf(f, "%f %f %f", &matrix2[i][0], &matrix2[i][1], &matrix2[i][2]);
		}
		summe = matrixAddierer(matrix1, matrix2);
		for (x = 0; x < 3; x++)
		{
			for (y = 0; y < 3; y++)
			{
				matrix1[x][y] = summe.array[x][y];
			}
		}
	}
	fclose(f);
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			printf("| %0.f |", summe.array[i][j]);
		}
		printf("\n");
	}
	getchar();
}