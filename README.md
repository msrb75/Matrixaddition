Name des Projekts: Matrixaddition 

Inhalt: Wir müssen zwei Matrizen beliebiger Dimension, miteinander addieren

Herausforderung und Lösungsansatz: Wir müssen einen Algorithmus entwickeln, zwei oder auch unendlich viele Matrizen miteinander
addieren. Zwei Matritzen sind gegeben, diese werden einer Funktion übergeben und dort addiert Die Funktion gibt dann eine Matrix 
zurück, welche bei weiteren Matrizen wieder in der Funktion übergeben wird um die nächste hinzu zu addieren!
Die Herausforderung wird vermutlich sein, die Matrizen in das Programm einlesen zu lassen. Außerdem ist es wichtig, 
die mathematischen Rechenregeln dabei zu beachten.